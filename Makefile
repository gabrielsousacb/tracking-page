bash:
	docker-compose exec  app "/bin/bash" 
install:
	docker-compose up -d
	docker-compose exec app sh -c "composer install"
	sudo chmod 777 -R www/
	docker-compose exec app sh -c "cp .env.example .env"
	docker-compose exec app sh -c "php artisan migrate"

test:
	docker-compose exec app sh -c "./vendor/bin/phpunit"
