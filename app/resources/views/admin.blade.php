<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hello Bulma!</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    </head>
    <body>
            <section class="section">
                <div id="app" class="container">
                        <h1>Stores</h1>
                        <ul>
                            <li></li>
                        </ul>
                </div>
            </section>



            <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
            <script>
                var app = new Vue({
                    el: '#app',
                    data: {
                        message: 'Olá Vue!'
                    }
                    });

            </script>
    </body>
</html>
