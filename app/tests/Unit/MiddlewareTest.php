<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MiddlewareTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreTrue()
    {

        $this->withHeaders([
            'store' => '1',
        ])->get(route('api.home'))
                ->assertStatus(200);
    }

    public function testStoreFalse()
    {

        $this->get(route('api.home'))
                ->assertStatus(401);
    }
}
