<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PagesExport;
use App\Page;
use App\User;

class ExportTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExportPagge()
    {
        $this->seed('DatabaseSeeder');

        $file =  Excel::download(new PagesExport( 1), 'temp.csv');

        if($file->getFile()->getRealPath())
        {
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }
}
