<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetKey()
    {
        $this->withHeaders([
            'store' => '1',
        ])->get(route('user.key'))
                ->assertStatus(201);

    }

    public function testSetEmail()
    {
        $user = User::create();

        $data = [
            'key' => User::encriprId($user->id),
            'email' => 'a@a.com',
        ];

        $this->withHeaders([
            'store' => '1',
        ])->post(route('user.email'), $data)
            ->assertStatus(200);
    }
}
