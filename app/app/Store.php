<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sftp_host',
        'sftp_user',
        'sftp_password',
        'sftp_port',
    ];



}
