<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'store_id',
        'device_id',
        'platform_id',
        'time_load',
        'sku',
        'sku_url',
        'sku_img'
    ];

    

}
