<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PagesExport;
use App\Store;
use Illuminate\Support\Facades\DB;

class ExportCsvFTP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:csvFtp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        print_r("Buscando Stores ... \n");
        $stores = Store::all();

        $s = null;


        foreach($stores as $store){
            print_r("Processando  ... \n");

            print_r("Upload store \n");
            print_r("Host " . $store->sftp_host . "\n");
            print_r("User " . $store->sftp_user . "\n");
            print_r("password " . $store->sftp_password . "\n");

            try{
                $s = $store;
                
                print_r("Gerando CSV ... \n");
                $file =  Excel::download(new PagesExport( $store->id), 'temp.csv');
                
                print_r("Conectaondo com  SFTP ... \n");

                
                $ftp = Storage::createSftpDriver([
                    'host'     => $store->sftp_host,
                    'username' => $store->sftp_user,
                    'password' => $store->sftp_password,
                ]);

                #$files = $ftp->files('/');

               # print_r($files);



                $time =  Carbon::now();
                $fileName = str_replace(':','-',$time."-pages.csv");

                print_r("Fazendo upload SFTP ... \n");
                $ftp->put('/Import/'.$fileName, file_get_contents($file->getFile()->getRealPath())  );

            }catch(\Exception $e){
                echo $e->getMessage();
                echo $e->getFile();
                echo $e->getLine();

                dd([
                    'host'     => $s->sftp_host,
                    'username' => $s->sftp_user,
                    'password' => $s->sftp_password,
                ]);
            }


        }
    }
}
