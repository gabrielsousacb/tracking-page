<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Crypt;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email'
    ];


    protected $hidden = ['id'];

    public static function decriptId($id)
    {
        return str_replace('quarks','',base64_decode($id));

    }

    public static function encriprId($id)
    {
        return base64_encode("quarks".$id);

    }

    public static function createAnonymous()
    {
        $user =  User::create([
            'email' => null
        ]);

        $user->key = User::encriprId($user->id);

        return $user;
    }

    public static function setKeyUser($key, $email){

        $id = User::decriptId($key);

        $user  = User::find($id);

        if($user){
            $user->email = $email;
            $user->save();

            return true;
        }

        return false;

    }
}
