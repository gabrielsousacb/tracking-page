<?php

namespace App\Http\Middleware;

use Closure;

class Store
{
    /**
     *
     * Verifica se todas as requisições vem o id da loja
     * Heder : store
     *
     */

    public function handle($request, Closure $next)
    {

        if(is_null($request->header('store'))) {
            return response(["erro" => "id da loja não encontrado"], '401');
        }

        return $next($request);
    }
}
