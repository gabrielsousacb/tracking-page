<?php

namespace App\Http\Controllers;

use App\Exports\PagesExport;
use App\Page;
use App\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
class PageController extends Controller
{

    public function tracking(Request $request)
    {
        $store = $request->header('store');

        $id = User::decriptId($request->input('key'));

        $data = [
            'user_id'       => $id,
            'name'          => $request->input('name'),
            'store_id'      => $store,
            'device_id'     => $request->input('device'),
            'platform_id'   => $request->input('platform'),
            'time_load'     => $request->input('time_load'),
        ];

        $page =  Page::create($data);

        if($page){
            return $page;
        }

    }

    public function tracking2(Request $request)
    {
        $store = $request->header('store');

        $id = User::decriptId($request->input('key'));

        $data = [
            'user_id'       => $id,
            'name'          => $request->input('name'),
            'store_id'      => $store,
            'device_id'     => $request->input('device'),
            'platform_id'   => $request->input('platform'),
            'sku'           => $request->input('sku'),
            'sku_url'       => $request->input('sku_url'),
            'sku_img'       => $request->input('sku_img'),
            'time_load'     => $request->input('time_load'),
        ];

        $page =  Page::create($data);

        if($page){
            return $page;
        }

    }

    public function exportCSV(Request $request){

        $store = $request->header('store');


        $file =  Excel::download(new PagesExport( $store), 'temp.csv');
        $store = Store::find( $store);


        $ftp = Storage::createSftpDriver([
            'host'     => $store->ftp_host,
            'username' => $store->ftp_user,
            'password' => $store->ftp_password,
        ]);


        $time =  Carbon::now();

        $ftp->put("upload/$time.csv", file_get_contents($file->getFile()->getRealPath())  );

        return array('status' => 200);
    }


}
