<?php

namespace App\Http\Controllers;

use App\Page;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{

    /**
     * Busca uma chave anonima
     *
     * Cria um usuario no banco de dados sem e-mail (anonimo)
     *
     * @return array
     */

    public function getKeyAnonymou(Request $request)
    {
        return User::createAnonymous();
    }



    /**
     *
     * Referencia Key com o E-email
     *
     */

    public function setKeyUser(Request $request)
    {
        // retorna se foi possivel atrelar o email ao id
        $return  = User::setKeyUser($request->input('key'),$request->input('email'));

        if($return){
            return response(['response' => 'ok'], 200);
        }else{
            return response(['erro' => 'erro'], 400);
        }
    }


}
