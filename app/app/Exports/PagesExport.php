<?php
namespace App\Exports;
use App\Device;
use App\Page;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpParser\ErrorHandler\Collecting;
class PagesExport implements FromCollection, WithHeadings
{
    public function __construct($store)
    {
        $this->store = $store;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $to = new \DateTime();
        $from = new \DateTime('-1 hour');
        //SELECT pages.id, users.id, users.email, pages.name, pages.sku,pages.sku_url, pages.sku_img, pages.created_at 
        //FROM `pages` 
        //INNER JOIN `users` ON pages.user_id=users.id 
        //WHERE users.email IS NOT NULL 
        //AND pages.created_at > '2020-03-11 14:11:07'

        $pages = DB::table('pages')
        ->join('users', 'pages.user_id', '=', 'users.id')
        ->select('pages.*', 'users.id', 'users.email')
        ->whereNotNull('users.email')
        ->whereBetween('pages.created_at', [$from, $to])
        ->get();


        $data = array();

        
        foreach($pages as $key => $page){
            print_r("Procesanndo $key de ". count($pages)." \n");
            array_push($data,
                [
                    "register_id"       =>  $page->id,
                    "register_session"  =>  User::encriprId($page->user_id),
                    "email"             =>  $page->email,
                    "url_visited"       =>  $page->name,
                    "sku"               => $page->sku,
                    "sku_url"           => $page->sku_url,
                    "sku_img"           => $page->sku_img,
                    "created"           =>  $page->created_at
                ]
            );
            
        }
        return collect($data);
    }
    public function headings(): array
    {
        return array('register_id','register_session','email','url_visited','sku','sku_url','sku_img','created');
    }

   
}