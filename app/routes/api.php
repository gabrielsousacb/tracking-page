<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::middleware(['store'])->group(function () {

    Route::get('/', function(){
        return response(['return' => 'ok'], 200);
    })->name('api.home');


    Route::prefix('v1')->group(function () {
        //pega uma key de um usuario anonime
        Route::get('/user/key', 'UserController@getKeyAnonymou')->name('user.key');

        //atrela uma key á um usuario
        Route::post('/user/email', 'UserController@setKeyUser')->name('user.email');

        //insere dados de rastreamento de paginas
        Route::post('/page/tracking', 'PageController@tracking')->name('page.tracking');
        Route::get('/page/export', 'PageController@exportCSV')->name('page.export');
    });

    Route::prefix('v2')->group(function () {
        //pega uma key de um usuario anonime
        Route::get('/user/key', 'UserController@getKeyAnonymou')->name('user.key');

        //atrela uma key á um usuario
        Route::post('/user/email', 'UserController@setKeyUser')->name('user.email');

        //insere dados de rastreamento de paginas
        Route::post('/page/tracking', 'PageController@tracking2')->name('page.tracking');
        Route::get('/page/export', 'PageController@exportCSV')->name('page.export');
    });
});
