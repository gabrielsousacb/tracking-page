<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Page;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
    ];
});


$factory->define(Page::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'store_id' => 1,
        'name' => 'home',
        'device_id' => 1,
    ];
});
