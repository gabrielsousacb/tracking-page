<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DeviceSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('devices')->insert([
            'name' => 'desktop',
        ]);

        DB::table('devices')->insert([
            'name' => 'smartphone',
        ]);


    }
}
