<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DeviceSeeder::class);
        $this->call(StoreSeeder::class);

        DB::table('users')->insert([
            'email' => Str::random(10).'@gmail.com',
        ]);

        DB::table('pages')->insert([
            'user_id' => 1,
            'store_id' => 1,
            'name' => 'home',
            'device_id' => 1,
        ]);


    }
}
